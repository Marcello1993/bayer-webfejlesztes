<?php
if (isset ($_POST['email']) == true && empty ($_POST['email']) == false) {
	$email = $_POST['email'];
	if (filter_var($email, FILTER_VALIDATE_EMAIL) == true) {
		echo 'Ez\'az email cím még nincsen ez adatbázisban.';
		} else {
			echo 'Ez az email cím már szerepel az adatbázisban.';
			}
	}
?>
<html lang="hu">
<head>
	<title>Bayer</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>



</head>
<body>
<div class="container">
	<div class="col-md-12">
		<div id="front">
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2"><img src="images/logo.png" class="img-responsive logo"><br><img src="images/bayer-logo.png" class="img-responsive" style="margin-left:auto;margin-right:auto;"></div>
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-10"><img src="images/kenocs.png" class="img-responsive logo" style="margin-left:auto;margin-right:auto;"></div>
		</div>
	</div>
</div>
<div class="container">
	<div class="col-md-12">
		<div class="col-md-12">
				<p class="header">Köszöntünk! Megérkeztél a Bepanthen Baby kenőcs terméktesztelésének regisztrációs oldalára.</p>
				<table class="table-responsive"width="100%"><tr><td style="border-bottom:1px solid #0090C5;"></td></tr></table>
				<p class="content">Arra kérünk, töltsd ki a regisztrációs kérdéseket, mi pedig emailben értesítünk majd téged a sikeres bejelentkezésről, valamint a 30 grammos Bepanthen Baby kenőcs és a meglepetés postázásának dátumáról. Siess, csak 100 helyünk van!</p>
		</div>
	</div>
</div>
<div class="container" style="padding-bottom:1%;">
  <button type="button" class="btn2 btn-info" data-toggle="collapse" data-target="#demo"><div style="font-size:2em;"><i class="fas fa-info-circle"></i></div></button>
  <div id="demo" class="collapse">
A Kampány során Adatkezelő Bepanthen Baby kenőcs nevű terméket biztosít tesztelés céljából, amely kozmetikai termék a pelenkakiütés megelőzése céljából a babák bőrének ápolására és védelmére szolgál. A Kampányt azért indítja az Adatkezelő, hogy legfeljebb 6 hónapos kisgyermeket nevelő szülőktől visszajelzést kapjon a terméke használatával összefüggésben. Hogy megelőzze a Kampány lebonyolítása során az esetleges visszaéléseket, illetve hogy a megfelelő személyektől kapjon a termékkel kapcsolatos visszajelzéseket, az Adatkezelő jogosult ellenőrizni az Érintettek által nyújtott adatok valódiságát és jogosult az Érintettek beazonosítására.
  </div>
</div>
<div class="container">
	<div class="col-lg-12"><form class="form" method="post" action="post.php">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
		<!-- FORM -->
			
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<label for="email" class="col-xs-11 col-sm-3 col-md-3 col-lg-3 col-form-label pad-t-5"><b>Név*:</b></label>
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<input type="text" class="form-control_contact" id="inlineFormInput" for="email" required name="nev" placeholder="Azonosítás céljából megadott és kezelt adat" value="<?php if(isset($_POST['nev'])) echo $_POST['nev']; ?>">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-t-1">
					<label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-form-label pad-t-5"><b>Cím*:</b></label>
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<input type="text" class="form-control_contact" id="inlineFormInput" for="email" required name="cim" placeholder="Kapcsolattartás céljából megadott és kezelt adat" value="<?php if(isset($_POST['cim'])) echo $_POST['cim']; ?>">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-t-1">
					<label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-form-label pad-t-5"><b>Irányítószám*:</b></label>
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<input type="number" class="form-control_contact" id="inlineFormInput" for="email" required name="iranyitoszam" placeholder="Kapcsolattartás céljából megadott és kezelt adat" value="<?php if(isset($_POST['iranyitoszam'])) echo $_POST['iranyitoszam']; ?>">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-t-1">
					<label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-form-label pad-t-5"><b>Telefonszám*:</b></label>
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<input type="tel" class="form-control_contact" id="inlineFormInput" for="email"  name="tel" placeholder="Kapcsolattartás céljából megadott és kezelt adat" value="<?php if(isset($_POST['tel'])) echo $_POST['tel']; ?>">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-t-1">
					<label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-form-label pad-t-5"><b>E-mail cím*:</b></label>
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<input type="email" class="form-control_contact" id="inlineFormInput"  for="email" required name="email" placeholder="Jelentkezés és kapcsolattartás céljából megadott és kezelt adat" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-t-1">
					<label for="text" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-form-label pad-t-5"><b>Háztartásban nevelt<br> 6 hónap alatti gyermek(ek) száma*:</b></label>
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 pad-t-gy">
					<input type="text" class="form-control_contact" id="inlineFormInput"  for="email" required name="gyermek" placeholder="Jelentkezés céljából megadott és kezelt adat" value="<?php if(isset($_POST['gyermek'])) echo $_POST['gyermek']; ?>">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-t-1">
					<label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-form-label pad-t-5"><b>Megjegyzés:</b></label>
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<textarea class="form-control_contact" id="inlineFormInput"  for="email" required name="megjegyzes" placeholder="Kapucsengő, emelet.." rows="4" style="height:200px;" value="<?php if(isset($_POST['megjegyzes'])) echo $_POST['megjegyzes']; ?>"></textarea>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 pad-t-tek">
				<img src="images/teknos.jpg" class="img-responsive" style="margin-left:auto;margin-right:auto;">
			</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pad-b-3" style="padding-top:2%;">
					<div class="form-check">
						<label class="form-check-label" for="email">
						<input type="checkbox" class="form-check-input" name="check01" required id="optionsRadios1" value="Hozzájárulok adataimat Jelentkezés céljából kezeljék, továbbá elfogadom az Adatvédelmi tájékoztatóban foglaltakat.">
						Hozzájárulok adataimat "Jelentkezés" céljából kezeljék, továbbá elfogadom az <a href="/documents/adatvedelmi_nyilatkozat.pdf" target="_blank">Adatvédelmi tájékoztatóban</a> foglaltakat.
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label" for="email">
						<input type="checkbox" class="form-check-input" name="check02" required id="optionsRadios3" value="Hozzájárulok adataimat Kapcsolattartás céljából kezeljék, továbbá elfogadom az Adatvédelemi tájékoztatóban foglaltakat.">
						Hozzájárulok adataimat "Kapcsolattartás" céljából kezeljék, továbbá elfogadom az <a href="/documents/adatvedelmi_nyilatkozat.pdf" target="_blank">Adatvédelmi tájékoztatóban</a> foglaltakat.
						</label>
					</div>
					<div class="form-check disabled pad-b-3">
						<label class="form-check-label" for="email">
						<input type="checkbox" class="form-check-input" name="check03" required id="optionsRadios4" value="Elfogadom a Részvételi szabályzatot." required/>
						Elfogadom a <a href="/documents/reszveteli_szabalyzat.pdf" target="_blank"><u>Részvételi szabályzatot.</u></a>
						</label>
					</div>
					<div class="form-group">
					<label class="control-label label_class" style="padding-top: 0px;" for="email">A *-gal jelölt mezők kitöltése kötelező! </label><input type="hidden" value="ok" id="ok" name="ok">
					</div>
					<p class="content">A kampányban való részvétellel vállalom, hogy az 1 hónapos tesztidőszak végén a részemre megküldött kérdőívet 3 napon belül kitöltöm.</p>
					<button type="submit" class="btn btn-info" id="submit" name="submit">REGISZTRÁLOK</button>
			</div>
					
			</form>
			</div>		

					
			</form>
			
		</div>
	</div>
<div class="container-fluid" style="background-color:#0090C5;">
	<div class="container">
		<div class="col-md-12">
		<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2"><img src="images/logo.png" class="img-responsive logo-footer" style="margin-left:auto;margin-right:auto;"></div>
				<div class="visible-xs col-xs-6"><img src="images/bayer-logo.png" class="img-responsive logo-footer" style="margin-left:auto;margin-right:auto;"></div>
		<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
		<p class="content-footer">Az adatkezelés hozzájáruláson alapul, amely hozzájárulás bármikor visszavonható, ugyanakkor a visszavonás nem érinti a visszavonást megelőzően végzett, hozzájáruláson alapuló adatkezelés jogszerűségét. A visszavonást kezdeményezni lehet az alábbi elérhetőségek valamelyikén (postacím: <b>1123 Budapest, Alkotás utca 50</b>., e-mail cím: <b><a href="mailto:adatvedelem@bayer.com">adatvedelem@bayer.com</a></b> tel:<b><a href="tel:+3614874281">+3614874281</a></b>), a Lebonyolító kapcsolattartójánál, vagy a regisztrációról kapott visszaigazoló e-mail alján lévő linkre történő kattintással. L.HU.MKT.CC.22.02.2018.1529.</p></div>

		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 pad-t-20 pad-b-20"><span id="scroll" class="pointer"><img src="images/scroll.png" class="img-responsive mar-l-au mar-r-au"></span>
		</div>
	</div>
</div>
</div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108076735-1"></script>
<script>
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
</script>
<script>
$("#scroll").click(function() {
    $('html, body').animate({
        scrollTop: $("#front").offset().top
    }, 2000);
});
</script>
</body>
</html>